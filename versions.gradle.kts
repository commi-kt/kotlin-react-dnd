//
// Plugins
// TODO: Make use of this rather than hardcoded value.
project.extra.set("kotlinJsPluginVersion", "1.3.70-eap-42")
//
// Kotlin
val kotlinVersion = "1.3.61"
project.extra.set("kotlinVersion", kotlinVersion)
//
// Wrappers
project.extra.set("kotlinJetBrainsWrappersVersion", "pre.91-kotlin-$kotlinVersion")
//
// Project
val versionReactDnd = "10.0.2"
val versionPre = 0
val projectVersion = "$versionReactDnd-pre.$versionPre-kotlin-$kotlinVersion"
//
println("projectVersion = $projectVersion")
//
project.extra.set("projectVersion", projectVersion)
