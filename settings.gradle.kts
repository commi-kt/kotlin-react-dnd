//
rootProject.name = "kotlin-react-dnd"
//
include(":examples")
include(":wrapper")
//
pluginManagement {
	repositories {
		//
		// Required for: id("com.jfrog.bintray")
		gradlePluginPortal()
		//
		// Required for: kotlin("js")
		maven("https://dl.bintray.com/kotlin/kotlin-eap")
		//
		// Required for dependencies of: kotlin("js")
		mavenCentral()
	}
}
