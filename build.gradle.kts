//
plugins {
	//
	// From: gradlePluginPortal()
	// https://github.com/bintray/gradle-bintray-plugin
	id("com.jfrog.bintray") version "1.8.4"
	//
	kotlin("js") version "1.3.70-eap-42" apply false
	//
	`maven-publish`
}
//
allprojects {
	//
	apply("$rootDir/versions.gradle.kts")
	//
	val projectVersion: String by project.extra
	//
	group = "tk.labyrinth"
	version = projectVersion
}
//
subprojects {
	//
	repositories {
		//
		// Required for transitive dependencies of: org.jetbrains:kotlin-react
		jcenter()
		//
		// Required for: org.jetbrains.kotlin:kotlin-source-map-loader
		//  It is used by js plugin.
		maven("https://dl.bintray.com/kotlin/kotlin-eap")
		//
		// Required for: org.jetbrains:kotlin-react
		maven("https://kotlin.bintray.com/kotlin-js-wrappers")
		//
		// Required for: org.jetbrains.kotlin:kotlin-compiler-embeddable
		//  FIXME: It is used by compileKotlin2Js task.
		// Required for: org.jetbrains.kotlin:kotlin-stdlib-js
		mavenCentral()
	}
	//
	extra["configurePublishingToBintray"] = {
		//
		apply {
			plugin("com.jfrog.bintray")
			plugin("maven-publish")
		}
		//
		val publicationArtifactId = rootProject.name
		val publicationName = "bundle"
		//
		bintray {
			user = System.getenv("BINTRAY_USERNAME")
			key = System.getenv("BINTRAY_API_KEY")
			pkg = PackageConfig().apply {
				repo = "kotlin-js-wrappers"
				name = publicationArtifactId
				setLicenses("MIT")
				setPublications(publicationName)
				vcsUrl = "https://gitlab.com/commi-kt/kotlin-react-dnd"
				//
				version = VersionConfig().apply {
					name = project.version as String
				}
			}
		}
		//
		publishing {
			publications.register(publicationName, MavenPublication::class) {
				from(components["kotlin"])
				this.artifactId = publicationArtifactId
			}
		}
	}
}
