# Kotlin React DnD

[![Bintray](https://img.shields.io/bintray/v/commitman/kotlin-js-wrappers/kotlin-react-dnd?label=Bintray)](https://bintray.com/commitman/kotlin-js-wrappers/kotlin-react-dnd/_latestVersion)

Kotlin Wrapper for [React DnD](https://react-dnd.github.io/react-dnd/) ([Githib](https://github.com/react-dnd/react-dnd/)).

## How to Use

- Add Kotlin dependency from [Bintray]((https://bintray.com/commitman/kotlin-js-wrappers/kotlin-react-dnd/_latestVersion));
- Add npm dependencies for **react-dnd** and **react-dnd-html5-backend**;
- See Examples [Build File](/examples/build.gradle.kts) on how to do it with Kotlin Gradle.

## Hooks & Functional Components

React DnD current solution uses hooks, which only works within Functional Components.
If you add hooks outside of FC you may encounter the following runtime error:
- Invalid hook call. Hooks can only be called inside of the body of a function component

See the links below on how to declare and use functional components in Kotlin:
- [Kotlin Examples](/examples/src/main/kotlin/tk/labyrinth/kotlin/reactdnd/examplehooks) (version of [original JS/TS examples]());
- [Original issue](https://github.com/JetBrains/kotlin-wrappers/issues/127#issuecomment-516328066).


## Trivia
- Using **org.jetbrains.kotlin.js** plugin instead of **kotlin2js**:
  - <https://discuss.kotlinlang.org/t/org-jetbrains-kotlin-js-vs-kotlin2js/14262/3>
- Gradle multi-module project:
  - <https://github.com/ilgonmic/kotlinjs-multi-module>
- Publish Kotlin Library on Bintray using Gradle Kotlin DSL and Travis CI:
  - <https://kotlinexpertise.com/kotlinlibrarydistibution/>

###### TODO

- Add deploy via Gitlab CI;
- Add DndScopes;
