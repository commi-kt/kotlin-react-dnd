package tk.labyrinth.kotlin.js

/**
 * Shorthand to use as an argument for [jsObject].
 */
typealias JsBuilder<T> = T.() -> Unit

/**
 * Variant of [kotlinext.js.jsObject] that does not require [T] to extend [Any].
 */
inline fun <T> jsObject(builder: JsBuilder<T>): T = (js("({})") as T).apply { builder() }
