package tk.labyrinth.kotlin.reactdnd.reactdnd.hooks

import tk.labyrinth.kotlin.js.JsBuilder
import tk.labyrinth.kotlin.js.jsObject
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragLayerMonitor
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag-layer](https://react-dnd.github.io/react-dnd/docs/api/use-drag-layer)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDragLayer.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDragLayer.ts)
 *
 * @see useDrag
 * @see useDragLayerWithObject
 * @see useDrop
 * @see rawUseDragLayer
 */
fun <DragObject : DragObjectWithType, CollectedProps> useDragLayerWithFunction(collect: (monitor: DragLayerMonitor<DragObject>) -> JsBuilder<CollectedProps>): CollectedProps {
	return rawUseDragLayer<DragObject, CollectedProps> { monitor -> jsObject(collect(monitor)) }
}

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag-layer](https://react-dnd.github.io/react-dnd/docs/api/use-drag-layer)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDragLayer.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDragLayer.ts)
 *
 * @see useDrag
 * @see useDragLayerWithFunction
 * @see useDrop
 * @see rawUseDragLayer
 */
fun <DragObject : DragObjectWithType, CollectedProps> useDragLayerWithObject(collect: (monitor: DragLayerMonitor<DragObject>) -> CollectedProps): CollectedProps {
	return rawUseDragLayer<DragObject, CollectedProps> { monitor -> collect(monitor) }
}
