package tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces

import tk.labyrinth.kotlin.reactdnd.dndcore.Identifier

external interface XYCoord {
	val x: Double
	val y: Double
}

operator fun XYCoord.component1(): Double {
	return x
}

operator fun XYCoord.component2(): Double {
	return y
}

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/drag-source-monitor](https://react-dnd.github.io/react-dnd/docs/api/drag-source-monitor)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/monitors.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/monitors.ts)
 *
 * TODO: Create common interface for monitors
 * TODO: Aggregate nullable properties into nullable object
 */
external interface DragSourceMonitor<DragObject : DragObjectWithType, DropResult : DropResultBase> {
	/**
	 * Returns true if no drag operation is in progress, and the owner's canDrag() returns true or is not defined.
	 */
	fun canDrag(): Boolean

	/**
	 * Returns true if a drag operation is in progress, and either the owner initiated the drag, or its isDragging() is defined and returns true.
	 */
	fun isDragging(): Boolean

	/**
	 * Returns a string or an ES6 symbol identifying the type of the current dragged item. Returns null if no item is being dragged.
	 */
	fun getItemType(): Identifier?

	/**
	 * Returns a plain object representing the currently dragged item. Every drag source must specify it by returning an object from its beginDrag() method.
	 * Returns null if no item is being dragged.
	 */
	fun getItem(): DragObject?

	/**
	 * Returns a plain object representing the last recorded drop result. The drop targets may optionally specify it by returning an object from their
	 * drop() methods. When a chain of drop() is dispatched for the nested targets, bottom up, any parent that explicitly returns its own result from drop()
	 * overrides the child drop result previously set by the child. Returns null if called outside endDrag().
	 */
	fun getDropResult(): DropResult?

	/**
	 *  Returns true if some drop target has handled the drop event, false otherwise. Even if a target did not return a drop result, didDrop() returns true.
	 * Use it inside endDrag() to test whether any drop target has handled the drop. Returns false if called outside endDrag().
	 */
	fun didDrop(): Boolean

	/**
	 * Returns the { x, y } client offset of the pointer at the time when the current drag operation has started. Returns null if no item is being dragged.
	 */
	fun getInitialClientOffset(): XYCoord?

	/**
	 * Returns the { x, y } client offset of the drag source component's root DOM node at the time when the current drag operation has started.
	 * Returns null if no item is being dragged.
	 */
	fun getInitialSourceClientOffset(): XYCoord?

	/**
	 * Returns the last recorded { x, y } client offset of the pointer while a drag operation is in progress. Returns null if no item is being dragged.
	 */
	fun getClientOffset(): XYCoord?

	/**
	 * Returns the { x, y } difference between the last recorded client offset of the pointer and the client offset when the current drag operation has started.
	 * Returns null if no item is being dragged.
	 */
	fun getDifferenceFromInitialOffset(): XYCoord?

	/**
	 * Returns the projected { x, y } client offset of the drag source component's root DOM node, based on its position at the time when the current drag operation has
	 * started, and the movement difference. Returns null if no item is being dragged.
	 */
	fun getSourceClientOffset(): XYCoord?

	/**
	 * Returns the ids of the potential drop targets.
	 *
	 * #### TODO: Replace with List?
	 */
	fun getTargetIds(): Array<Identifier>
}

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/drop-target-monitor](https://react-dnd.github.io/react-dnd/docs/api/drop-target-monitor)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/monitors.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/monitors.ts)
 */
external interface DropTargetMonitor<DragObject : DragObjectWithType, DropResult : DropResultBase> {
	/**
	 * Returns true if there is a drag operation in progress, and the owner's canDrop() returns true or is not defined.
	 */
	fun canDrop(): Boolean

	/**
	 * Returns true if there is a drag operation in progress, and the pointer is currently hovering over the owner.
	 * You may optionally pass { shallow: true } to strictly check whether only the owner is being hovered, as opposed to a nested target.
	 *
	 * #### TODO: Add options as argument.
	 */
	fun isOver(): Boolean

	/**
	 * Returns a string or an ES6 symbol identifying the type of the current dragged item. Returns null if no item is being dragged.
	 */
	fun getItemType(): Identifier?

	/**
	 * Returns a plain object representing the currently dragged item. Every drag source must specify it by returning an object from
	 * its beginDrag() method. Returns null if no item is being dragged.
	 */
	fun getItem(): DragObject?

	/**
	 * Returns a plain object representing the last recorded drop result. The drop targets may optionally specify it by returning an
	 * object from their drop() methods. When a chain of drop() is dispatched for the nested targets, bottom up, any parent that explicitly
	 * returns its own result from drop() overrides the drop result previously set by the child. Returns null if called outside drop().
	 */
	fun getDropResult(): DropResult?

	/**
	 *  Returns true if some drop target has handled the drop event, false otherwise. Even if a target did not return a drop result,
	 * didDrop() returns true. Use it inside drop() to test whether any nested drop target has already handled the drop. Returns false
	 * if called outside drop().
	 */
	fun didDrop(): Boolean

	/**
	 * Returns the { x, y } client offset of the pointer at the time when the current drag operation has started. Returns null if no item
	 * is being dragged.
	 */
	fun getInitialClientOffset(): XYCoord?

	/**
	 * Returns the { x, y } client offset of the drag source component's root DOM node at the time when the current drag operation has started.
	 * Returns null if no item is being dragged.
	 */
	fun getInitialSourceClientOffset(): XYCoord?

	/**
	 * Returns the last recorded { x, y } client offset of the pointer while a drag operation is in progress. Returns null if no item is being dragged.
	 */
	fun getClientOffset(): XYCoord?

	/**
	 * Returns the { x, y } difference between the last recorded client offset of the pointer and the client offset when current the drag operation has
	 * started. Returns null if no item is being dragged.
	 */
	fun getDifferenceFromInitialOffset(): XYCoord?

	/**
	 * Returns the projected { x, y } client offset of the drag source component's root DOM node, based on its position at the time when the current
	 * drag operation has started, and the movement difference. Returns null if no item is being dragged.
	 */
	fun getSourceClientOffset(): XYCoord?
}

external interface DragLayerMonitor<DragObject : DragObjectWithType> {
	/**
	 * Returns true if a drag operation is in progress. Returns false otherwise.
	 */
	fun isDragging(): Boolean

	/**
	 * Returns a string or an ES6 symbol identifying the type of the current dragged item.
	 * Returns null if no item is being dragged.
	 */
	fun getItemType(): Identifier?

	/**
	 * Returns a plain object representing the currently dragged item.
	 * Every drag source must specify it by returning an object from its beginDrag() method.
	 * Returns null if no item is being dragged.
	 */
	fun getItem(): DragObject?

	/**
	 * Returns the { x, y } client offset of the pointer at the time when the current drag operation has started.
	 * Returns null if no item is being dragged.
	 */
	fun getInitialClientOffset(): XYCoord?

	/**
	 * Returns the { x, y } client offset of the drag source component's root DOM node at the time when the current
	 * drag operation has started. Returns null if no item is being dragged.
	 */
	fun getInitialSourceClientOffset(): XYCoord?

	/**
	 * Returns the last recorded { x, y } client offset of the pointer while a drag operation is in progress.
	 * Returns null if no item is being dragged.
	 */
	fun getClientOffset(): XYCoord?

	/**
	 * Returns the { x, y } difference between the last recorded client offset of the pointer and the client
	 * offset when current the drag operation has started. Returns null if no item is being dragged.
	 */
	fun getDifferenceFromInitialOffset(): XYCoord?

	/**
	 * Returns the projected { x, y } client offset of the drag source component's root DOM node, based on its
	 * position at the time when the current drag operation has started, and the movement difference.
	 * Returns null if no item is being dragged.
	 */
	fun getSourceClientOffset(): XYCoord?
}
