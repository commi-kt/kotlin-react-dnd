@file:JsModule("react-dnd-html5-backend")

package tk.labyrinth.kotlin.reactdnd.html5backend

import org.w3c.dom.HTMLImageElement

external fun getEmptyImage(): HTMLImageElement
