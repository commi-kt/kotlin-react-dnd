@file:JsModule("react-dnd")

package tk.labyrinth.kotlin.reactdnd.reactdnd.hooks

import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragLayerMonitor
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag-layer](https://react-dnd.github.io/react-dnd/docs/api/use-drag-layer)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDragLayer.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDragLayer.ts)
 *
 * @see useDragLayerWithObject
 * @see useDragLayerWithFunction
 */
@JsName("useDragLayer")
external fun <DragObject : DragObjectWithType, CollectedProps> rawUseDragLayer(collect: (monitor: DragLayerMonitor<DragObject>) -> CollectedProps): CollectedProps
