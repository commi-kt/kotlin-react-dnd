package tk.labyrinth.kotlin.reactdnd.reactdnd.hooks

import kotlinext.js.jsObject
import react.RRef
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.ConnectDragPreview
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragSourceHookSpec
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.RawDragSourceHookSpec

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag](https://react-dnd.github.io/react-dnd/docs/api/use-drag)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrag.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrag.ts)
 *
 * @see useDragLayerWithFunction
 * @see useDragLayerWithObject
 * @see useDrop
 * @see rawUseDrag
 */
fun <DragObject : DragObjectWithType, DropResult : DropResultBase, CollectedProps> useDrag(spec: DragSourceHookSpec<DragObject, DropResult, CollectedProps>.() -> Unit): UseDragResult<CollectedProps> {
	val rawSpecObject = jsObject<RawDragSourceHookSpec<DragObject, DropResult, CollectedProps>> { }
	spec(DragSourceHookSpec(rawSpecObject))
	//
	val jsTriple = rawUseDrag(rawSpecObject)
	//
	val collectedProps = jsTriple[0] as CollectedProps
	val dragSource = jsTriple[1].unsafeCast<RRef>()
	val dragPreview = jsTriple[2] as ConnectDragPreview
	//
	return UseDragResult(collectedProps, dragSource, dragPreview)
}

data class UseDragResult<CollectedProps>(
	val collectedProps: CollectedProps,
	val connectDragSource: RRef,
	val connectDragPreview: ConnectDragPreview
)
