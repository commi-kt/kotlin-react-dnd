@file:JsModule("react-dnd")

package tk.labyrinth.kotlin.reactdnd.reactdnd.common

import react.Component
import react.RState

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/common/DndProvider.tsx](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/common/DndProvider.tsx)
 *
 * @see DndProvider
 */
@JsName("DndProvider")
external object RawDndProvider : Component<DndProviderProps, RState> {
	override fun render(): dynamic
}
