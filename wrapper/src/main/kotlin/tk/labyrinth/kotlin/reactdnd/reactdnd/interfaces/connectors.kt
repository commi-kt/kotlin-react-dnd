package tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces

import react.ReactElement

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/connectors.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/connectors.ts)
 */
typealias DragElementWrapper<Options> = (
	elementOrNode: dynamic,
	options: Options?
) -> ReactElement?

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/connectors.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/connectors.ts)
 */
typealias ConnectDragSource = DragElementWrapper<DragSourceOptions>

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/connectors.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/connectors.ts)
 */
typealias ConnectDragPreview = DragElementWrapper<DragPreviewOptions>
