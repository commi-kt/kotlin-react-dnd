package tk.labyrinth.kotlin.reactdnd.dndcore

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/dnd-core/src/interfaces.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/dnd-core/src/interfaces.ts)
 *
 * #### TODO: Add asSymbol, isString, isSymbol methods.
 * #### TODO: Probably find better way to define union type "string | symbol".
 */
external interface Identifier

/**
 * Interprets this as [String].
 */
fun Identifier.asString(): String = this.unsafeCast<String>()

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/dnd-core/src/interfaces.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/dnd-core/src/interfaces.ts)
 */
external interface Backend
