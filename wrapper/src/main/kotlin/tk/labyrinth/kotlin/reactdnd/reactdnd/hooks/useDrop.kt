@file:JsModule("react-dnd")

package tk.labyrinth.kotlin.reactdnd.reactdnd.hooks

import react.RDependenciesArray
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.RawDropTargetHookSpec

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drop](https://react-dnd.github.io/react-dnd/docs/api/use-drop)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrop.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrop.ts)
 *
 * @see useDrop
 */
@JsName("useDrop")
external fun <DragObject : DragObjectWithType, DropResult : DropResultBase, CollectedProps> rawUseDrop(spec: RawDropTargetHookSpec<DragObject, DropResult, CollectedProps>): RDependenciesArray
