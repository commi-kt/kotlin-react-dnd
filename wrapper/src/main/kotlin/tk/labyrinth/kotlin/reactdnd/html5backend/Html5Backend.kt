@file:JsModule("react-dnd-html5-backend")

package tk.labyrinth.kotlin.reactdnd.html5backend

import tk.labyrinth.kotlin.reactdnd.dndcore.Backend

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/html5-backend/src/HTML5Backend.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/html5-backend/src/HTML5Backend.ts)
 */
@JsName("default")
external object Html5Backend : Backend
