@file:JsModule("react-dnd")

package tk.labyrinth.kotlin.reactdnd.reactdnd.hooks

import react.RDependenciesArray
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.RawDragSourceHookSpec

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag](https://react-dnd.github.io/react-dnd/docs/api/use-drag)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrag.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrag.ts)
 *
 * @see useDrag
 */
@JsName("useDrag")
external fun <DragObject : DragObjectWithType, DropResult : DropResultBase, CollectedProps> rawUseDrag(spec: RawDragSourceHookSpec<DragObject, DropResult, CollectedProps>): RDependenciesArray
