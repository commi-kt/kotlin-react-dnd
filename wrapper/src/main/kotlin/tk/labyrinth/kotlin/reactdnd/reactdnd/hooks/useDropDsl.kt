package tk.labyrinth.kotlin.reactdnd.reactdnd.hooks

import kotlinext.js.jsObject
import react.RRef
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropTargetHookSpec
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.RawDropTargetHookSpec

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drop](https://react-dnd.github.io/react-dnd/docs/api/use-drop)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrop.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/hooks/useDrop.ts)
 *
 * @see useDrag
 * @see useDragLayerWithFunction
 * @see useDragLayerWithObject
 * @see rawUseDrop
 */
fun <DragObject : DragObjectWithType, DropResult : DropResultBase, CollectedProps> useDrop(spec: DropTargetHookSpec<DragObject, DropResult, CollectedProps>.() -> Unit): UseDropResult<CollectedProps> {
	val rawSpecObject = jsObject<RawDropTargetHookSpec<DragObject, DropResult, CollectedProps>> { }
	spec(DropTargetHookSpec(rawSpecObject))
	//
	val jsTuple = rawUseDrop(rawSpecObject)
	//
	val collectedProps = jsTuple[0] as CollectedProps
	val dropTarget = jsTuple[1].unsafeCast<RRef>()
	//
	return UseDropResult(collectedProps, dropTarget)
}

data class UseDropResult<CollectedProps>(
	val collectedProps: CollectedProps,
	val connectedDropTarget: RRef
)
