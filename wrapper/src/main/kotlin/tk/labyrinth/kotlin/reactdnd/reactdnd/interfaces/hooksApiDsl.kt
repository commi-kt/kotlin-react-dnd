package tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces

import tk.labyrinth.kotlin.js.JsBuilder
import tk.labyrinth.kotlin.js.jsObject

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag#specification-object-members](https://react-dnd.github.io/react-dnd/docs/api/use-drag#specification-object-members)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/hooksApi.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/hooksApi.ts)
 */
class DragSourceHookSpec<DragObject : DragObjectWithType, DropResult : DropResultBase, CollectedProps>(private val rawSpec: RawDragSourceHookSpec<DragObject, DropResult, CollectedProps>) {
	/**
	 * A plain javascript item describing the data being dragged.
	 * This is the only information available to the drop targets about the drag
	 * source so it's important to pick the minimal data they need to know.
	 *
	 * You may be tempted to put a reference to the component or complex object here,
	 * but you shouldx try very hard to avoid doing this because it couples the
	 * drag sources and drop targets. It's a good idea to use something like
	 * { id: props.id }
	 */
	fun item(item: DragObject) {
		rawSpec.item = item
	}

	/**
	 * A plain javascript item describing the data being dragged.
	 * This is the only information available to the drop targets about the drag
	 * source so it's important to pick the minimal data they need to know.
	 *
	 * You may be tempted to put a reference to the component or complex object here,
	 * but you shouldx try very hard to avoid doing this because it couples the
	 * drag sources and drop targets. It's a good idea to use something like
	 * { id: props.id }
	 */
	fun item(item: JsBuilder<DragObject>) {
		rawSpec.item = jsObject(item)
	}

	/**
	 * The drag source options
	 */
	fun options(options: DragSourceOptions?) {
		rawSpec.options = options
	}

	/**
	 * DragPreview options
	 */
	fun previewOptions(previewOptions: DragPreviewOptions?) {
		rawSpec.previewOptions = previewOptions
	}

	/**
	 * When the dragging starts, beginDrag is called. If an object is returned from this function it will overide the default dragItem
	 *
	 * #### TODO: Add overrides for "DragObject | undefined"
	 */
	fun begin(begin: ((monitor: DragSourceMonitor<DragObject, DropResult>) -> Unit)?) {
		rawSpec.begin = begin
	}

	/**
	 * Optional.
	 * When the dragging stops, endDrag is called. For every beginDrag call, a corresponding endDrag call is guaranteed.
	 * You may call monitor.didDrop() to check whether or not the drop was handled by a compatible drop target. If it was handled,
	 * and the drop target specified a drop result by returning a plain object from its drop() method, it will be available as
	 * monitor.getDropResult(). This method is a good place to fire a Flux action. Note: If the component is unmounted while dragging,
	 * component parameter is set to be null.
	 *
	 * ######
	 * #### API Note: Argument function only receives monitor instead of item/monitor as in React DnD. You may obtain item via [monitor.getItem()][DragSourceMonitor.getItem].
	 */
	fun end(end: ((monitor: DragSourceMonitor<DragObject, DropResult>) -> Unit)?) {
		rawSpec.end = end?.let { { _, monitor -> end(monitor) } }
	}

	/**
	 * Optional.
	 * Use it to specify whether the dragging is currently allowed. If you want to always allow it, just omit this method.
	 * Specifying it is handy if you'd like to disable dragging based on some predicate over props. Note: You may not call
	 * monitor.canDrag() inside this method.
	 */
	fun canDrag(canDrag: Boolean?) {
		rawSpec.canDrag = canDrag
	} //?: boolean | ((monitor: DragSourceMonitor) => boolean)

	/**
	 * Optional.
	 * Use it to specify whether the dragging is currently allowed. If you want to always allow it, just omit this method.
	 * Specifying it is handy if you'd like to disable dragging based on some predicate over props. Note: You may not call
	 * monitor.canDrag() inside this method.
	 */
	fun canDrag(canDrag: ((monitor: DragSourceMonitor<DragObject, DropResult>) -> Boolean)?) {
		rawSpec.canDrag = canDrag
	}

	/**
	 * Optional.
	 * By default, only the drag source that initiated the drag operation is considered to be dragging. You can
	 * override this behavior by defining a custom isDragging method. It might return something like props.id === monitor.getItem().id.
	 * Do this if the original component may be unmounted during the dragging and later “resurrected” with a different parent.
	 * For example, when moving a card across the lists in a Kanban board, you want it to retain the dragged appearance—even though
	 * technically, the component gets unmounted and a different one gets mounted every time you move it to another list.
	 *
	 * Note: You may not call monitor.isDragging() inside this method.
	 */
	fun isDragging(isDragging: ((monitor: DragSourceMonitor<DragObject, DropResult>) -> Boolean)?) {
		rawSpec.isDragging = isDragging
	}

	/**
	 * A function to collect rendering properties
	 */
	fun collect(collect: ((monitor: DragSourceMonitor<DragObject, DropResult>) -> JsBuilder<CollectedProps>)?) {
		rawSpec.collect = collect?.let { { monitor -> jsObject(collect(monitor)) } }
	}
}

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/docs/api/use-drag#specification-object-members](https://react-dnd.github.io/react-dnd/docs/api/use-drag#specification-object-members)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/hooksApi.ts](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/interfaces/hooksApi.ts)
 */
class DropTargetHookSpec<DragObject : DragObjectWithType, DropResult : DropResultBase, CollectedProps>(private val rawSpec: RawDropTargetHookSpec<DragObject, DropResult, CollectedProps>) {
	/**
	 * The kinds of dragItems this dropTarget accepts
	 */
	fun accept(accept: String) {
		rawSpec.accept = accept
	}

	/**
	 * The drop target optinos
	 */
	fun options(options: dynamic) {
		rawSpec.options = options
	}

	/**
	 * Optional.
	 * Called when a compatible item is dropped on the target. You may either return undefined, or a plain object.
	 * If you return an object, it is going to become the drop result and will be available to the drag source in its
	 * endDrag method as monitor.getDropResult(). This is useful in case you want to perform different actions
	 * depending on which target received the drop. If you have nested drop targets, you can test whether a nested
	 * target has already handled drop by checking monitor.didDrop() and monitor.getDropResult(). Both this method and
	 * the source's endDrag method are good places to fire Flux actions. This method will not be called if canDrop()
	 * is defined and returns false.
	 *
	 * ######
	 * #### API Note: Argument function only receives monitor instead of item/monitor as in React DnD. You may obtain item via [monitor.getItem()][DropTargetMonitor.getItem].
	 *
	 * #### TODO: Add an overload that returns object and not builder (in case we already have an object to return).
	 */
	fun dropObject(drop: ((monitor: DropTargetMonitor<DragObject, DropResult>) -> JsBuilder<DropResult>)?) {
		rawSpec.drop = drop?.let { { _, monitor -> jsObject(drop(monitor)) } }
	}

	/**
	 * Optional.
	 * Called when a compatible item is dropped on the target. You may either return undefined, or a plain object.
	 * If you return an object, it is going to become the drop result and will be available to the drag source in its
	 * endDrag method as monitor.getDropResult(). This is useful in case you want to perform different actions
	 * depending on which target received the drop. If you have nested drop targets, you can test whether a nested
	 * target has already handled drop by checking monitor.didDrop() and monitor.getDropResult(). Both this method and
	 * the source's endDrag method are good places to fire Flux actions. This method will not be called if canDrop()
	 * is defined and returns false.
	 *
	 * ######
	 * #### API Note: Argument function only receives monitor instead of item/monitor as in React DnD. You may obtain item via [monitor.getItem()][DropTargetMonitor.getItem].
	 */
	fun dropUndefined(drop: ((monitor: DropTargetMonitor<DragObject, DropResult>) -> Unit)?) {
		rawSpec.drop = drop?.let {
			{ _, monitor ->
				drop(monitor)
				undefined
			}
		}
	}

	/**
	 * Optional.
	 * Called when an item is hovered over the component. You can check monitor.isOver({ shallow: true }) to test whether
	 * the hover happens over just the current target, or over a nested one. Unlike drop(), this method will be called even
	 * if canDrop() is defined and returns false. You can check monitor.canDrop() to test whether this is the case.
	 */
	fun hover(hover: ((monitor: DropTargetMonitor<DragObject, DropResult>) -> Unit)?) {
		rawSpec.hover = hover?.let { { _, monitor -> hover(monitor) } }
	}

	/**
	 * Optional. Use it to specify whether the drop target is able to accept the item. If you want to always allow it, just
	 * omit this method. Specifying it is handy if you'd like to disable dropping based on some predicate over props or
	 * monitor.getItem(). Note: You may not call monitor.canDrop() inside this method.
	 */
	fun canDrop(canDrop: ((monitor: DropTargetMonitor<DragObject, DropResult>) -> Boolean)?) {
		rawSpec.canDrop = canDrop?.let { { _, monitor -> canDrop(monitor) } }
	}

	/**
	 * A function to collect rendering properties
	 */
	fun collect(collect: ((monitor: DropTargetMonitor<DragObject, DropResult>) -> JsBuilder<CollectedProps>)?) {
		rawSpec.collect = collect?.let { { monitor -> jsObject(collect(monitor)) } }
	}
}

external interface DropResultBase {
	/**
	 * Default value is "move".
	 */
	var dropEffect: dynamic
}
