package tk.labyrinth.kotlin.reactdnd.reactdnd.common

import react.RBuilder
import react.RHandler
import react.RProps
import tk.labyrinth.kotlin.reactdnd.dndcore.Backend

/**
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/common/DndProvider.tsx](https://github.com/react-dnd/react-dnd/blob/master/packages/core/react-dnd/src/common/DndProvider.tsx)
 *
 * @see RawDndProvider
 */
fun RBuilder.DndProvider(backend: Backend, handler: (RHandler<DndProviderProps>) = {}) = child(RawDndProvider::class) {
	attrs.backend = backend
	handler.invoke(this)
}

external interface DndProviderProps : RProps {
	var backend: Backend
}
