import org.jetbrains.kotlin.utils.addToStdlib.cast

plugins {
	kotlin("js")
}
//
dependencies {
	val kotlinVersion: String by project.extra
	val kotlinJetBrainsWrappersVersion: String by project.extra
	//
	// From: repositories: mavenCentral()
	implementation(kotlin("stdlib-js", kotlinVersion))
	//
	// From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react
	implementation("org.jetbrains", "kotlin-react", "16.9.0-$kotlinJetBrainsWrappersVersion")
}
//
kotlin {
	target {
		browser {
			useCommonJs()
		}
	}
}
//
extra.get("configurePublishingToBintray").cast<() -> Unit>().invoke()
