//
plugins {
	kotlin("js")
}
//
dependencies {
	val kotlinVersion: String by project.extra
	val kotlinJetBrainsWrappersVersion: String by project.extra
	//
	implementation(project(":wrapper"))
	//
	// From: repositories: mavenCentral()
	implementation("org.jetbrains.kotlin:kotlin-stdlib-js:$kotlinVersion")
	//
	// From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react
	implementation("org.jetbrains:kotlin-react:16.9.0-$kotlinJetBrainsWrappersVersion")
	//
	//From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react-dom
	implementation("org.jetbrains:kotlin-react-dom:16.9.0-$kotlinJetBrainsWrappersVersion")
	//
	// From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-styled
	implementation("org.jetbrains:kotlin-styled:1.0.0-$kotlinJetBrainsWrappersVersion")
	//
	//	// Required for: org.jetbrains:kotlin-styled
	implementation((npm("inline-style-prefixer")))
	//
	// Required for: org.jetbrains:kotlin-react
	implementation((npm("react")))
	//
	//	// Required for: tk.labyrinth:kotlin-react-dnd
	implementation((npm("react-dnd")))
	implementation((npm("react-dnd-html5-backend")))
	//	//
	//	// Required for: org.jetbrains:kotlin-react-dom
	implementation((npm("react-dom")))
	//	//
	//	// Required for: org.jetbrains:kotlin-styled
	implementation((npm("styled-components")))
}
//
kotlin {
	target {
		browser {
			useCommonJs()
		}
	}
}
