package tk.labyrinth.kotlin.reactdnd.examplehooks.dustbin.singletarget

import kotlinx.css.Clear
import kotlinx.css.Overflow
import kotlinx.css.clear
import kotlinx.css.overflow
import react.RBuilder
import react.dom.div
import styled.css
import styled.styledDiv

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/examples/dustbin/single-target](https://react-dnd.github.io/react-dnd/examples/dustbin/single-target)
 * - TS Source:
 *    - [https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/01-dustbin/single-target](https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/01-dustbin/single-target)
 */
fun RBuilder.Example() {
	div {
		styledDiv {
			css {
				overflow = Overflow.hidden
				clear = Clear.both
			}
			//
			Dustbin()
		}
		styledDiv {
			css {
				overflow = Overflow.hidden
				clear = Clear.both
			}
			//
			Box(name = "Glass")
			Box(name = "Banana")
			Box(name = "Paper")
		}
	}
}
