package tk.labyrinth.kotlin.reactdnd.examplehooks

import org.w3c.dom.Document
import react.dom.render
import tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer.App
import kotlin.browser.document

fun init(document: Document) {
	document.getElementById("root")?.let { root ->
		render(root) {
			// TODO: List all examples.
			App()
		}
	}
}

fun main() {
	if (document.body != null) {
		init(document)
	} else {
		document.addEventListener("DOMContentLoaded", {
			init(document)
		})
	}
}
