package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import react.RBuilder
import react.dom.div
import tk.labyrinth.kotlin.reactdnd.html5backend.Html5Backend
import tk.labyrinth.kotlin.reactdnd.reactdnd.common.DndProvider

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/examples/drag-around/custom-drag-layer](https://react-dnd.github.io/react-dnd/examples/drag-around/custom-drag-layer)
 * - TS Sources:
 *    - [https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/02-drag-around/custom-drag-layer](https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/02-drag-around/custom-drag-layer)
 */
fun RBuilder.App() {
	div("App") {
		DndProvider(Html5Backend) {
			Example()
		}
	}
}
