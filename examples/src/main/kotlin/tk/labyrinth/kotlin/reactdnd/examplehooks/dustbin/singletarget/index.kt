package tk.labyrinth.kotlin.reactdnd.examplehooks.dustbin.singletarget

import react.RBuilder
import react.dom.div
import tk.labyrinth.kotlin.reactdnd.html5backend.Html5Backend
import tk.labyrinth.kotlin.reactdnd.reactdnd.common.DndProvider

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/examples/dustbin/single-target](https://react-dnd.github.io/react-dnd/examples/dustbin/single-target)
 * - TS Sources:
 *    - [https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/01-dustbin/single-target](https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/01-dustbin/single-target)
 */
fun RBuilder.App() {
	div("App") {
		DndProvider(Html5Backend) {
			Example()
		}
	}
}
