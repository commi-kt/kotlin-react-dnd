package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlinx.css.Display
import kotlinx.css.PointerEvents
import kotlinx.css.Position
import kotlinx.css.display
import kotlinx.css.height
import kotlinx.css.left
import kotlinx.css.pct
import kotlinx.css.pointerEvents
import kotlinx.css.position
import kotlinx.css.properties.transform
import kotlinx.css.properties.translate
import kotlinx.css.px
import kotlinx.css.ruleSet
import kotlinx.css.top
import kotlinx.css.width
import kotlinx.css.zIndex
import react.RBuilder
import styled.css
import styled.styledDiv
import tk.labyrinth.kotlin.css.InlineRuleSet
import tk.labyrinth.kotlin.css.inlineStyles
import tk.labyrinth.kotlin.react.functional
import tk.labyrinth.kotlin.reactdnd.dndcore.asString
import tk.labyrinth.kotlin.reactdnd.reactdnd.hooks.useDragLayerWithObject
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.XYCoord
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.component1
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.component2

private val layerStyles = ruleSet {
	position = Position.fixed
	pointerEvents = PointerEvents.none
	zIndex = 100
	left = 0.px
	top = 0.px
	width = 100.pct
	height = 100.pct
}

private fun getItemStyles(
	initialOffset: XYCoord?,
	currentOffset: XYCoord?,
	isSnapToGrid: Boolean
): InlineRuleSet {
	if (initialOffset == null || currentOffset == null) {
		return {
			display = Display.none
		}
	}
	//
	var (x, y) = currentOffset
	//
	if (isSnapToGrid) {
		x -= initialOffset.x
		y -= initialOffset.y
		//
		val (newLeft, newTop) = snapToGrid(x, y)
		x = newLeft
		y = newTop
		//
		x += initialOffset.x
		y += initialOffset.y
	}
	//
	// TODO: Make transform variable again!
	return {
		transform {
			translate(x.px, y.px)
			// TODO: WebkitTransform: transform
		}
	}
}

private data class DragLayerProps(
	val itemType: String?,
	val isDragging: Boolean,
	val item: DraggableBoxDragObject?,
	val initialOffset: XYCoord?,
	val currentOffset: XYCoord?
)

fun RBuilder.CustomDragLayer(snapToGrid: Boolean) = functional {
	val (
		itemType,
		isDragging,
		item,
		initialOffset,
		currentOffset
	) = useDragLayerWithObject<DraggableBoxDragObject, DragLayerProps> { monitor ->
		DragLayerProps(
			item = monitor.getItem(),
			itemType = monitor.getItemType()?.asString(),
			initialOffset = monitor.getInitialSourceClientOffset(),
			currentOffset = monitor.getSourceClientOffset(),
			isDragging = monitor.isDragging()
		)
	}
	//
	fun RBuilder.renderItem() {
		when (itemType) {
			ItemTypes.BOX -> BoxDragPreview(item!!.title)
			else -> {
				// no-op
			}
		}
	}
	//
	// FIXME: Unnecessary suppression due to Kotlin inspection lacks "Comments count as content" feature.
	@Suppress("ControlFlowWithEmptyBody")
	if (!isDragging) {
		// no-op
	} else {
		styledDiv {
			css(layerStyles)
			//
			styledDiv {
				inlineStyles(getItemStyles(initialOffset, currentOffset, snapToGrid))
				//
				renderItem()
			}
		}
	}
}
