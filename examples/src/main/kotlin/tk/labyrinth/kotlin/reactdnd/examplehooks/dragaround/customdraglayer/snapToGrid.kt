package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlin.math.round

fun snapToGrid(x: Double, y: Double): Pair<Double, Double> {
	val snappedX = round(x / 32) * 32
	val snappedY = round(y / 32) * 32
	return snappedX to snappedY
}
