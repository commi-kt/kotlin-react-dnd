package tk.labyrinth.kotlin.reactdnd.examplehooks.dustbin.singletarget

import kotlinx.css.Color
import kotlinx.css.Float
import kotlinx.css.TextAlign
import kotlinx.css.backgroundColor
import kotlinx.css.color
import kotlinx.css.float
import kotlinx.css.fontSize
import kotlinx.css.height
import kotlinx.css.lineHeight
import kotlinx.css.marginBottom
import kotlinx.css.marginRight
import kotlinx.css.padding
import kotlinx.css.properties.LineHeight
import kotlinx.css.rem
import kotlinx.css.textAlign
import kotlinx.css.width
import react.RBuilder
import styled.css
import styled.styledDiv
import tk.labyrinth.kotlin.react.functional
import tk.labyrinth.kotlin.reactdnd.reactdnd.hooks.useDrop
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase

fun RBuilder.Dustbin() {
	functional {
		val (collectedProps, drop) = useDrop<DragObjectWithType, DustbinDropResult, DustbinDropProps> {
			accept(ItemTypes.BOX)
			dropObject {
				{
					name = "Dustbin"
				}
			}
			collect { monitor ->
				{
					isOver = monitor.isOver()
					canDrop = monitor.canDrop()
				}
			}
		}
		//
		val isActive = collectedProps.canDrop && collectedProps.isOver
		//
		styledDiv {
			css {
				height = 12.rem
				width = 12.rem
				marginRight = 1.5.rem
				marginBottom = 1.5.rem
				color = Color.white
				padding(1.rem)
				textAlign = TextAlign.center
				fontSize = 1.rem
				lineHeight = LineHeight.normal
				float = Float.left
				//
				backgroundColor = when {
					isActive -> Color.darkGreen
					collectedProps.canDrop -> Color.darkKhaki
					else -> Color("#222")
				}
			}
			//
			ref = drop
			//
			+if (isActive) "Release to drop" else "Drag a box here"
		}
	}
}

external interface DustbinDropProps {
	var isOver: Boolean
	var canDrop: Boolean
}

external interface DustbinDropResult : DropResultBase {
	var name: String
}
