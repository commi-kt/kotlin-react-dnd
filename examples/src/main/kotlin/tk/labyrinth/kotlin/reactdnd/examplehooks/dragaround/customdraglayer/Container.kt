package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlinext.js.jsObject
import kotlinx.css.Position
import kotlinx.css.border
import kotlinx.css.height
import kotlinx.css.position
import kotlinx.css.px
import kotlinx.css.ruleSet
import kotlinx.css.width
import react.RBuilder
import react.RProps
import react.functionalComponent
import react.useState
import styled.css
import styled.styledDiv
import tk.labyrinth.kotlin.react.useCallback
import tk.labyrinth.kotlin.reactdnd.reactdnd.hooks.useDrop
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.math.round

private val styles = ruleSet {
	width = 300.px
	height = 300.px
	border = "1px solid black"
	position = Position.relative
}

data class Box(
	val top: Double,
	val left: Double,
	val title: String
)

fun RBuilder.renderBox(item: Box, key: String) {
	DraggableBox(DraggableBoxProps(id = key, title = item.title, top = item.top, left = item.left))
}

private interface ContainerProps : RProps {
	var snapToGrid: Boolean
}

private operator fun ContainerProps.component1(): Boolean {
	return snapToGrid
}

private val Container = functionalComponent<ContainerProps> { (snapToGrid) ->
	val (boxes, setBoxes) = useState(mapOf(
		"a" to Box(top = 20.0, left = 80.0, title = "Drag me around"),
		"b" to Box(top = 180.0, left = 20.0, title = "Drag me too")
	))
	//
	val moveBox = useCallback({ id: String, left: Double, top: Double ->
		setBoxes(boxes.let {
			val current = it[id] ?: error("")
			return@let it.plus(id to current.copy(left = left, top = top))
		})
	}, arrayOf(boxes))
	//
	val (_, drop) = useDrop<DraggableBoxDragObject, DropResultBase, dynamic> {
		accept(ItemTypes.BOX)
		dropUndefined { monitor ->
			val item = monitor.getItem()!!
			//
			val delta = monitor.getDifferenceFromInitialOffset()!!
			//
			var left = round(item.left + delta.x)
			var top = round(item.top + delta.y)
			if (snapToGrid) {
				val (newLeft, newTop) = snapToGrid(left, top)
				left = newLeft
				top = newTop
			}
			moveBox(item.id, left, top)
		}
	}
	//
	styledDiv {
		ref = drop
		//
		css(styles)
		//
		boxes.forEach { (key, value) -> renderBox(value, key) }
	}
}

fun RBuilder.Container(snapToGrid: Boolean) {
	child(Container, jsObject<ContainerProps> { this.snapToGrid = snapToGrid }, {})
}
