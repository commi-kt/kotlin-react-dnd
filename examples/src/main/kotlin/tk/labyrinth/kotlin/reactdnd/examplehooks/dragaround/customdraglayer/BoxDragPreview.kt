package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlinext.js.jsObject
import kotlinx.css.Display
import kotlinx.css.display
import kotlinx.css.properties.Angle
import kotlinx.css.properties.rotate
import kotlinx.css.properties.transform
import kotlinx.css.ruleSet
import react.RBuilder
import react.RProps
import react.functionalComponent
import react.useEffectWithCleanup
import react.useState
import styled.css
import styled.styledDiv
import tk.labyrinth.kotlin.types.node.clearInterval
import tk.labyrinth.kotlin.types.node.setInterval

private val styles = ruleSet {
	display = Display.inlineBlock
	transform {
		rotate(Angle("-7deg"))
	}
	// FIXME: WebkitTransform: 'rotate(-7deg)',
}

private interface BoxDragPreviewProps : RProps {
	var title: String
}

private operator fun BoxDragPreviewProps.component1(): String {
	return title
}

private val BoxDragPreview = functionalComponent<BoxDragPreviewProps> { (title) ->
	val (tickTock, setTickTock) = useState(false)
	//
	useEffectWithCleanup(listOf(tickTock)) {
		val interval = setInterval({ setTickTock(!tickTock) }, 500)
		return@useEffectWithCleanup { clearInterval(interval) }
	}
	//
	styledDiv {
		css(styles)
		//
		Box(title, tickTock)
	}
}

fun RBuilder.BoxDragPreview(title: String) {
	child(BoxDragPreview, jsObject<BoxDragPreviewProps> { this.title = title }, {})
}
