package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlinext.js.jsObject
import kotlinx.css.Position
import kotlinx.css.height
import kotlinx.css.opacity
import kotlinx.css.position
import kotlinx.css.properties.transform
import kotlinx.css.properties.translate3d
import kotlinx.css.px
import react.RBuilder
import react.useEffect
import styled.styledDiv
import tk.labyrinth.kotlin.css.InlineRuleSet
import tk.labyrinth.kotlin.css.inlineStyles
import tk.labyrinth.kotlin.react.functional
import tk.labyrinth.kotlin.reactdnd.html5backend.getEmptyImage
import tk.labyrinth.kotlin.reactdnd.reactdnd.hooks.useDrag
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DropResultBase

private fun getStyles(left: Number, top: Number, isDragging: Boolean): InlineRuleSet {
	// TODO: Make transform variable again!
	//	val transform = "translate3d(${left}px, ${top}px, 0)"
	return {
		position = Position.absolute
		transform {
			translate3d(left.px, top.px, 0.px)
		}
		// TODO: WebkitTransform: transform
		// IE fallback: hide the real node using CSS when dragging
		// because IE will ignore our custom "empty image" drag preview.
		opacity = if (isDragging) 0 else 1
		if (isDragging) {
			height = 0.px
		}
	}
}

private external interface DragProps {
	var isDragging: Boolean
}

data class DraggableBoxProps(
	val id: String,
	val title: String,
	val left: Double,
	val top: Double
)

fun RBuilder.DraggableBox(props: DraggableBoxProps) = functional {
	val (id, title, left, top) = props
	//
	val (collectedProps, drag, preview) = useDrag<DraggableBoxDragObject, DropResultBase, DragProps> {
		item {
			type = ItemTypes.BOX
			this.id = id
			this.left = left
			this.top = top
			this.title = title
		}
		collect { monitor ->
			{
				isDragging = monitor.isDragging()
			}
		}
	}
	//
	useEffect {
		preview(getEmptyImage(), jsObject { captureDraggingState = true })
	}
	//
	styledDiv {
		ref = drag
		//
		inlineStyles(getStyles(left, top, collectedProps.isDragging))
		//
		Box(title = title)
	}
}

external interface DraggableBoxDragObject : DragObjectWithType {
	var id: String
	var top: Double
	var left: Double
	var title: String
}
