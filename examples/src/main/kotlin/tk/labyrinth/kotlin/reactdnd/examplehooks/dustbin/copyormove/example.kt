package tk.labyrinth.kotlin.reactdnd.examplehooks.dustbin.copyormove

import kotlinx.css.Clear
import kotlinx.css.Overflow
import kotlinx.css.clear
import kotlinx.css.overflow
import react.RBuilder
import react.dom.div
import styled.css
import styled.styledDiv

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/examples/dustbin/copy-or-move](https://react-dnd.github.io/react-dnd/examples/dustbin/copy-or-move)
 * - TS Sources:
 *    - [https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/01-dustbin/copy-or-move](https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/01-dustbin/copy-or-move)
 */
fun RBuilder.Example() {
	div {
		styledDiv {
			css {
				overflow = Overflow.hidden
				clear = Clear.both
			}
			//
			Dustbin(allowedDropEffect = DropEffect.ANY)
			Dustbin(allowedDropEffect = DropEffect.COPY)
			Dustbin(allowedDropEffect = DropEffect.MOVE)
		}
		styledDiv {
			css {
				overflow = Overflow.hidden
				clear = Clear.both
			}
			//
			Box(name = "Glass")
			Box(name = "Banana")
			Box(name = "Paper")
		}
	}
}
