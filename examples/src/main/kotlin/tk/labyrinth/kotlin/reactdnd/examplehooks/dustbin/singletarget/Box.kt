package tk.labyrinth.kotlin.reactdnd.examplehooks.dustbin.singletarget

import kotlinx.css.Color
import kotlinx.css.Cursor
import kotlinx.css.Float
import kotlinx.css.backgroundColor
import kotlinx.css.border
import kotlinx.css.cursor
import kotlinx.css.float
import kotlinx.css.marginBottom
import kotlinx.css.marginRight
import kotlinx.css.opacity
import kotlinx.css.padding
import kotlinx.css.rem
import react.RBuilder
import react.ReactElement
import styled.css
import styled.styledDiv
import tk.labyrinth.kotlin.react.functional
import tk.labyrinth.kotlin.reactdnd.reactdnd.hooks.useDrag
import tk.labyrinth.kotlin.reactdnd.reactdnd.interfaces.DragObjectWithType
import kotlin.browser.window

fun RBuilder.Box(name: String): ReactElement {
	return functional {
		val (collectedProps, drag) = useDrag<BoxDragObject, DustbinDropResult, BoxDragProps> {
			item {
				this.name = name
				type = ItemTypes.BOX
			}
			end { monitor ->
				// This code differs from the original js example.
				// Instead of using item argument from 2-args function it uses 1-arg function and retrieves item from monitor.
				val item = monitor.getItem()
				val dropResult = monitor.getDropResult()
				if (item != null && dropResult != null) {
					window.alert("You dropped ${item.name} into ${dropResult.name}!")
				}
			}
			collect { monitor ->
				{
					isDragging = monitor.isDragging()
				}
			}
		}
		//
		styledDiv {
			css {
				border = "1px dashed gray"
				backgroundColor = Color.white
				padding(0.5.rem, 1.rem)
				marginRight = 1.5.rem
				marginBottom = 1.5.rem
				cursor = Cursor.move
				float = Float.left
				//
				opacity = if (collectedProps.isDragging) 0.4 else 1
			}
			//
			ref = drag
			//
			+name
		}
	}
}

external interface BoxDragObject : DragObjectWithType {
	var name: String
}

external interface BoxDragProps {
	var isDragging: Boolean
}
