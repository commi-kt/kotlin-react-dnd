package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlinext.js.jsObject
import kotlinx.html.InputType
import kotlinx.html.id
import kotlinx.html.js.onChangeFunction
import react.RBuilder
import react.RProps
import react.dom.br
import react.dom.div
import react.dom.input
import react.dom.label
import react.dom.p
import react.dom.small
import react.functionalComponent
import react.useCallback
import react.useState
import tk.labyrinth.kotlin.html.reactHtmlFor

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/examples/drag-around/custom-drag-layer](https://react-dnd.github.io/react-dnd/examples/drag-around/custom-drag-layer)
 * - TS Sources:
 *    - [https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/02-drag-around/custom-drag-layer](https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/02-drag-around/custom-drag-layer)
 */
private val Example = functionalComponent<RProps> {
	val (snapToGridAfterDrop, setSnapToGridAfterDrop) = useState(false)
	val (snapToGridWhileDragging, setSnapToGridWhileDragging) = useState(false)
	//
	val handleSnapToGridAfterDropChange = useCallback({
		setSnapToGridAfterDrop(!snapToGridAfterDrop)
	}, arrayOf(snapToGridAfterDrop))
	//
	val handleSnapToGridWhileDraggingChange = useCallback({
		setSnapToGridWhileDragging(!snapToGridWhileDragging)
	}, arrayOf(snapToGridWhileDragging))
	//
	div {
		Container(snapToGridAfterDrop)
		CustomDragLayer(snapToGridWhileDragging)
		//
		p {
			// FIXME: Find out why React thinks inputs are uncontrolled.
			label {
				attrs.reactHtmlFor = "snapToGridWhileDragging"
				//
				input {
					attrs.id = "snapToGridWhileDragging"
					attrs.type = InputType.checkBox
					attrs.checked = snapToGridWhileDragging
					attrs.onChangeFunction = { handleSnapToGridWhileDraggingChange() }
				}
				small {
					+"Snap to grid while dragging"
				}
			}
			br {}
			label {
				attrs.reactHtmlFor = "snapToGridAfterDrop"
				//
				input {
					attrs.id = "snapToGridAfterDrop"
					attrs.type = InputType.checkBox
					attrs.checked = snapToGridAfterDrop
					attrs.onChangeFunction = { handleSnapToGridAfterDropChange() }
				}
				small {
					+"Snap to grid after drop"
				}
			}
		}
	}
}

/**
 * - Docs:
 *    - [https://react-dnd.github.io/react-dnd/examples/drag-around/custom-drag-layer](https://react-dnd.github.io/react-dnd/examples/drag-around/custom-drag-layer)
 * - TS Sources:
 *    - [https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/02-drag-around/custom-drag-layer](https://github.com/react-dnd/react-dnd/tree/master/packages/documentation/examples-hooks/src/02-drag-around/custom-drag-layer)
 */
fun RBuilder.Example() {
	child(Example, jsObject<RProps> { }, {})
}
