package tk.labyrinth.kotlin.reactdnd.examplehooks.dragaround.customdraglayer

import kotlinx.css.Color
import kotlinx.css.Cursor
import kotlinx.css.backgroundColor
import kotlinx.css.border
import kotlinx.css.cursor
import kotlinx.css.padding
import kotlinx.css.rem
import kotlinx.css.ruleSet
import react.RBuilder
import styled.css
import styled.inlineStyles
import styled.styledDiv
import tk.labyrinth.kotlin.react.functional

private val styles = ruleSet {
	border = "1px dashed gray"
	padding(0.5.rem, 1.rem)
	cursor = Cursor.move
}

fun RBuilder.Box(title: String, yellow: Boolean? = null) = functional {
	//
	styledDiv {
		css(styles)
		//
		inlineStyles { backgroundColor = if (yellow == true) Color.yellow else Color.white }
		//
		+title
	}
}
