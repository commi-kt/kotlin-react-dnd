package tk.labyrinth.kotlin.css

import kotlinx.css.StyledElement
import styled.StyledDOMBuilder
import styled.toStyle

typealias InlineRuleSet = InlineStyleBuilder.() -> Unit

fun inlineRuleSet(ruleSet: InlineRuleSet) = ruleSet
/**
 * Based on [kotlinx.css.CSSBuilder]. Adds possibility to add inline style sets with plus operator.
 */
open class InlineStyleBuilder : StyledElement() {
	/**
	 * Operator identical to [kotlinx.css.CSSBuilder.unaryPlus] (the one with **operator fun RuleSet.unaryPlus()** signature).
	 */
	operator fun InlineRuleSet.unaryPlus() = this()
}

/**
 * Based on [styled.inlineStyles].
 */
fun StyledDOMBuilder<*>.inlineStyles(prefix: Boolean = true, ruleSet: InlineRuleSet) {
	val styles = InlineStyleBuilder()
	ruleSet(styles)
	setProp("style", styles.toStyle(prefix))
}

/**
 * Based on [styled.css] (the one with __StyledBuilder<*>.css(handler: RuleSet)__ signature).
 */
fun StyledDOMBuilder<*>.inlineStyles(ruleSet: InlineRuleSet) {
	inlineStyles(true, ruleSet)
}
