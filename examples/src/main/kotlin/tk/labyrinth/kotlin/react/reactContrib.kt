package tk.labyrinth.kotlin.react

import react.RBuilder
import react.RProps
import react.ReactElement
import react.child
import react.functionalComponent
import tk.labyrinth.kotlin.types.react.memo

fun RBuilder.functional(func: RBuilder.() -> Unit): ReactElement {
	return functional<RProps> { func.invoke(this) }
}

fun <P : RProps> RBuilder.functional(func: RBuilder.(props: P) -> Unit): ReactElement {
	return child(functionalComponent(func))
}

fun <P : RProps> RBuilder.functionalMemo(func: RBuilder.(props: P) -> Unit): ReactElement {
	return memo(child(functionalComponent(func)))
}
