@file:JsModule("react")

package tk.labyrinth.kotlin.react

import react.RDependenciesArray

/**
 *
 */
external fun <P0> useCallback(callback: (P0) -> Unit, dependencies: RDependenciesArray): (P0) -> Unit

external fun <P0, P1> useCallback(callback: (P0, P1) -> Unit, dependencies: RDependenciesArray): (P0, P1) -> Unit
external fun <P0, P1, P2> useCallback(callback: (P0, P1, P2) -> Unit, dependencies: RDependenciesArray): (P0, P1, P2) -> Unit
@JsName("useCallback")
external fun <P0, P1, P2, R> useCallbackWithResult(callback: (P0, P1, P2) -> Unit, dependencies: RDependenciesArray): (P0, P1, P2) -> R
