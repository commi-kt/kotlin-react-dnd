package tk.labyrinth.kotlin.react

import react.RSetState
import tk.labyrinth.kotlin.js.JsBuilder
import tk.labyrinth.kotlin.js.jsObject

fun <T> useStateWithJsBuilder(valueBuilder: JsBuilder<T>): Pair<T, RSetState<T>> {
	return react.useState(jsObject(valueBuilder))
}


