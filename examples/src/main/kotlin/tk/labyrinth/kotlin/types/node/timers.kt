package tk.labyrinth.kotlin.types.node

/**
 * - TS Source:
 *    - function setInterval(callback: (...args: any[]) => void, ms: number, ...args: any[]): NodeJS.Timeout;
 *    - [https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/node/v12/timers.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/node/v12/timers.d.ts)
 */
external fun setInterval(callback: (args: dynamic) -> Unit, ms: Number, vararg args: dynamic): dynamic

/**
 * - TS Source:
 *    - function clearInterval(intervalId: NodeJS.Timeout): void;
 *    - [https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/node/v12/timers.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/node/v12/timers.d.ts)
 */
external fun clearInterval(intervalId: dynamic)
