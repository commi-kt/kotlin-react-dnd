@file:JsModule("react")

package tk.labyrinth.kotlin.types.react

import react.ReactElement

/**
 * - JS Source:
 *    - [https://github.com/facebook/react/blob/master/packages/react/src/memo.js](https://github.com/facebook/react/blob/master/packages/react/src/memo.js)
 * - TS Source:
 *    - function memo<P extends object>(
 *    - function memo<T extends ComponentType<any>>(
 *    - [https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react/index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react/index.d.ts)
 */
external fun memo(type: dynamic, compare: dynamic = definedExternally): ReactElement
//external fun <Props> memo(type: dynamic, compare: ((oldProps: Props, newProps: Props) -> Boolean)? = definedExternally): dynamic
