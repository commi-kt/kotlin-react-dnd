package tk.labyrinth.kotlin.html

import kotlinx.html.LABEL
import kotlinx.html.attributes.Attribute
import kotlinx.html.attributes.StringAttribute

/**
 * Copy of similar val from [LABEL.htmlFor] file.
 */
internal val attributeStringString: Attribute<String> = StringAttribute()
/**
 * [LABEL.htmlFor] is translated into "for" and React gives the following message:
 * - Warning: Invalid DOM property `for`. Did you mean `htmlFor`?
 *
 * Use this property to avoid such warnings.
 */
var LABEL.reactHtmlFor: String
	get() = attributeStringString.get(this, "htmlFor")
	set(newValue) {
		attributeStringString.set(this, "htmlFor", newValue)
	}
